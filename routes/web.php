<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'secure-headers'], function (){
    Auth::routes();
    Route::resource('/home','HomeController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/msg', 'HomeController@msg')->name('msg');
    Route::get('/all_users', 'HomeController@all_users')->name('all_users');
    Route::get('/settings', 'HomeController@settings')->name('settings');
    Route::put('/updateAuth', 'HomeController@updateAuth')->name('updateAuth');
    Route::delete('/deleteAuth', 'HomeController@deleteAuth')->name('home');
    Route::delete('/ForceDeleteUser/{id}', 'HomeController@ForceDeleteUser')->name('all_users');
    Route::post('/send', 'HomeController@send')->name('home');
    Route::delete('/deletemsg/{id}', 'HomeController@deletemsg')->name('msg');
    Route::delete('/deleteuser/{id}', 'HomeController@deleteuser')->name('all_users');
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/', 'VisitorController@StoreVisitor');

});


