@extends('layouts.app')

@section('content')
<div class="uk-container">

<!----------------Uikit-------------------->
    <div class="uk-card uk-card-default uk-card-body uk-width-1-2@m" style="margin: auto;">
        <h3 class="uk-card-title">{{ __('Login') }}</h3>
        @if (session('deleteAuth'))
                    <div class="uk-alert-warning" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        {{ session('deleteAuth') }}
                    </div>
                @endif
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="uk-margin">
                <div class="uk-inline">
                    <span class="uk-form-icon" uk-icon="icon: user"></span>
                    <input name="email" class="uk-input uk-width-xxlarge{{ $errors->has('email') ? ' uk-form-danger' : '' }}"
                     type="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
            </div>

            <div class="uk-margin">
                <div class="uk-inline">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                    <input name="password" class="uk-input uk-width-xxlarge{{ $errors->has('password') ? ' uk-form-danger' : '' }}"
                           type="password" required>
                    @if ($errors->has('password'))
                        <div class="uk-alert-danger" uk-alert>
                            <a class="uk-alert-close" uk-close></a>
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="uk-margin">
                <div class="uk-inline">
                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        <label><input class="uk-checkbox" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}</label>
                    </div>
                </div>
            </div>
            <div class="uk-margin">
                <div class="uk-inline">
                    <button class="uk-button uk-button-primary" type="submit">{{ __('Login') }}</button>
                    <a class="uk-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </div>

        </form>
    </div>

</div>
@endsection
