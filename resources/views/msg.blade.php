@extends('layouts.app')

@section('content')
    <div class="uk-container">

        <div  class="uk-card uk-card-default uk-card-body">
            <div class="uk-card-title">Messages</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="uk-alert-success" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        {{ session('status') }}
                    </div>
                @endif

            <!-------------Messages----------------->
                @if(count($messages)>0)
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-table-shrink">Name</th>
                                <th class="uk-table-shrink">Email</th>
                                <th class="uk-table-expand">Message</th>
                                <th class="uk-table-shrink">Phone number</th>
                                <th class="uk-table-shrink">Sent on</th>
                                <th class="uk-table-shrink">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $msg)
                                <tr>
                                    <td><input class="uk-checkbox" type="checkbox"></td>
                                    <td class="uk-text-nowrap"></td>
                                    <td class="uk-text-nowrap"></td>
                                    <td>{{$msg->message}}</td>
                                    <td class="uk-text-nowrap">{{$msg->mobile}}</td>
                                    <td class="uk-text-nowrap">{{$msg->created_at}}</td>
                                    <td class="uk-text-nowrap">
                                        <button class="uk-button uk-button-danger" href="#delete{{$msg->id}}"
                                                uk-toggle>Delete</button>
                                    </td>
                                </tr>
                                <!----------Modals---delete----->
                                <div id="delete{{$msg->id}}" uk-modal>
                                    <div class="uk-modal-dialog">
                                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                        <div class="uk-modal-header">
                                            <h2 class="uk-modal-title">Delete Messages</h2>
                                        </div>
                                        <div class="uk-modal-body">
                                            <p>Are you sure to delete this message? </p>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <form method="POST" action="{{url('deletemsg/'.$msg->id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                                <button class="uk-button uk-button-primary" type="submit">Confirm</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <p>You did not send any message. <a href="#send">Send Fee SMS</a></p>
                @endif

            </div>



        </div>

    </div>
@endsection
