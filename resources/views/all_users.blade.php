
@extends('layouts.app')

@section('content')
    <div class="uk-container">

        <div  class="uk-card uk-card-default uk-card-body">
            <div class="uk-card-title">Active Users</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="uk-alert-success" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        {{ session('status') }}
                    </div>
                @endif

            <!-------------Users----------------->
          @if(count($users)>0)
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-table-shrink">Name</th>
                                <th class="uk-table-shrink">Email</th>
                                <th class="uk-table-shrink">Phone number</th>
                                <th class="uk-table-shrink">ip</th>
                                <th class="uk-table-shrink">Latitude</th>
                                <th class="uk-table-shrink">Longitude</th>
                                <th class="uk-table-shrink">Created at</th>
                                <th class="uk-table-shrink">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td><input class="uk-checkbox" type="checkbox"></td>
                                    <td class="uk-text-nowrap">{{$user->name}}</td>
                                    <td class="uk-text-nowrap">{{$user->email}}</td>
                                    <td class="uk-text-nowrap">{{$user->mobile}}</td>
                                    <td class="uk-text-nowrap">{{$user->ip}}</td>
                                    <td class="uk-text-nowrap">{{$user->lat}}</td>
                                    <td class="uk-text-nowrap">{{$user->long}}</td>
                                    <td class="uk-text-nowrap">{{$user->created_at}}</td>
                                    <td class="uk-text-nowrap">
                                        <button class="uk-button uk-button-danger" href="#delete{{$user->id}}"
                                                uk-toggle>Delete</button>
                                    </td>
                                </tr>
                                <!----------Modals---delete----->
                                <div id="delete{{$user->id}}" uk-modal>
                                    <div class="uk-modal-dialog">
                                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                        <div class="uk-modal-header">
                                            <h2 class="uk-modal-title">Delete Users</h2>
                                        </div>
                                        <div class="uk-modal-body">
                                            <p>Are you sure to delete this User? </p>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <form method="POST" action="{{url('deleteuser/'.$user->id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                                <button class="uk-button uk-button-primary" type="submit">Confirm</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
           @endif
           <!--ççççççççççDeleted usersççççççççç-->
           @if(count($deletedUsers)>0)
           <hr>
           <h3>Deleted Users</h3>
           <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-table-shrink">Name</th>
                                <th class="uk-table-shrink">Email</th>
                                <th class="uk-table-shrink">Phone number</th>
                                <th class="uk-table-shrink">ip</th>
                                <th class="uk-table-shrink">Latitude</th>
                                <th class="uk-table-shrink">Longitude</th>
                                <th class="uk-table-shrink">Created at</th>
                                <th class="uk-table-shrink">Deleted at</th>
                                <th class="uk-table-shrink">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($deletedUsers as $user)
                                <tr>
                                    <td><input class="uk-checkbox" type="checkbox"></td>
                                    <td class="uk-text-nowrap">{{$user->name}}</td>
                                    <td class="uk-text-nowrap">{{$user->email}}</td>
                                    <td class="uk-text-nowrap">{{$user->mobile}}</td>
                                    <td class="uk-text-nowrap">{{$user->ip}}</td>
                                    <td class="uk-text-nowrap">{{$user->lat}}</td>
                                    <td class="uk-text-nowrap">{{$user->long}}</td>
                                    <td class="uk-text-nowrap">{{$user->created_at}}</td>
                                    <td class="uk-text-nowrap">{{$user->deleted_at}}</td>
                                    <td class="uk-text-nowrap">
                                        <button class="uk-button uk-button-danger" href="#ForceDelete{{$user->id}}"
                                                uk-toggle>Delete</button>
                                    </td>
                                </tr>
                                <!----------Modals---delete----->
                                <div id="ForceDelete{{$user->id}}" uk-modal>
                                    <div class="uk-modal-dialog">
                                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                        <div class="uk-modal-header">
                                            <h2 class="uk-modal-title">Delete Users</h2>
                                        </div>
                                        <div class="uk-modal-body">
                                            <p>Are you sure to delete definitely this User? </p>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <form method="POST" action="{{url('ForceDeleteUser/'.$user->id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                                <button class="uk-button uk-button-primary" type="submit">Confirm</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
           @else
                    <p>There is no user available.</p>
          @endif
        <!--=============Visitors============-->
           <hr>
           <h3>All Visitors</h3>
            @if(count($visitors)>0)
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                            <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-table-shrink">Id</th>
                                <th class="uk-table-shrink">City</th>
                                <th class="uk-table-shrink">Ip</th>
                                <th class="uk-table-shrink">Latitude</th>
                                <th class="uk-table-shrink">Longitude</th>
                                <th class="uk-table-shrink">Visited at</th>
                                <th class="uk-table-shrink">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visitors as $visitor)
                                <tr>
                                    <td><input class="uk-checkbox" type="checkbox"></td>
                                    <td class="uk-text-nowrap">{{$visitor->id}}</td>
                                    <td class="uk-text-nowrap">{{$visitor->city}}</td>
                                    <td class="uk-text-nowrap">{{$visitor->ip}}</td>
                                    <td class="uk-text-nowrap">{{$visitor->lat}}</td>
                                    <td class="uk-text-nowrap">{{$visitor->long}}</td>
                                    <td class="uk-text-nowrap">{{$visitor->created_at}}</td>
                                    <td class="uk-text-nowrap">
                                        <button class="uk-button uk-button-danger" href="#delete{{$visitor->id}}visit"
                                                uk-toggle>Delete</button>
                                    </td>
                                </tr>
                                <!----------Modals---delete----->
                                <div id="delete{{$visitor->id}}visit" uk-modal>
                                    <div class="uk-modal-dialog">
                                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                        <div class="uk-modal-header">
                                            <h2 class="uk-modal-title">Delete Visitors</h2>
                                        </div>
                                        <div class="uk-modal-body">
                                            <p>Are you sure to delete this Visitor? </p>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <form method="POST" action="{{url('deleteVisitor/'.$visitor->id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                                <button class="uk-button uk-button-primary" type="submit">Confirm</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
           @endif



            </div>



        </div>

    </div>
@endsection
