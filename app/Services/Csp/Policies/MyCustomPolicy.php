<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/02/19
 * Time: 20:28
 */

namespace App\Services\Csp\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;

class MyCustomPolicy extends Basic
{
    public function configure()
    {
        parent::configure();

        $this->addDirective(Directive::STYLE, 'https://fonts.googleapis.com/');
    }
}