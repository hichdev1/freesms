<?php

namespace App\Http\Controllers;

use App\Numberslist;
use Illuminate\Http\Request;

class NumberslistController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Numberslist  $numberslist
     * @return \Illuminate\Http\Response
     */
    public function show(Numberslist $numberslist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Numberslist  $numberslist
     * @return \Illuminate\Http\Response
     */
    public function edit(Numberslist $numberslist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Numberslist  $numberslist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Numberslist $numberslist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Numberslist  $numberslist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Numberslist $numberslist)
    {
        //
    }
}
