<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Location;
use App\Visitor;

class VisitorController extends Controller
{
    

public function StoreVisitor(){
    $visitor= new Visitor();
    $visitor->ip =$this->get_ip();
    $visitor->lat=!empty($this->data_geoloc()->latitude)?$this->data_geoloc()->latitude:0;
    $visitor->long=!empty($this->data_geoloc()->longitude)?$this->data_geoloc()->longitude:0;
    $visitor->city=!empty($this->data_geoloc()->cityName)?$this->data_geoloc()->cityName:'';
    $visitor->save();
    //dd($this->data_geoloc());
    return redirect('login');

} 

protected function get_ip(){

        // IP si internet partagé
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
    return $_SERVER['HTTP_CLIENT_IP'];
    }
    // IP derrière un proxy
    elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Sinon : IP normale
    else {
    return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
    }
}

/*
get location by ip
*/
protected function data_geoloc(){
    // return $data = geoip()->getLocation($this->get_ip());
    return $data = Location::get($this->get_ip());
}




}//---End-Class
