<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Numberslist;
use App\Message;
use App\User;
use App\Visitor;
use Illuminate\Support\Facades\Auth;
use Twilio\Exceptions\TwilioException;
use Validator;
use Nexmo\Laravel\Facade\Nexmo;
use Twilio\Rest\Client;
use Location;
use geoip;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $numbers=Numberslist::where('user_id',Auth::id())->orderBy('created_at','desc')->get();;
        //$messages=Message::where('user_id',Auth::id())->orderBy('created_at','desc')->get();;
        $user=Auth::user();
        return view('home',compact('numbers','user'));
    }


    public function msg()
    {
        $messages=Message::where('user_id',Auth::id())->orderBy('created_at','desc')->get();
        $user=Auth::user();
        return view('msg',compact('user','messages'));
    }


      public function all_users()
    {
        if (Auth::user()->is_admin==1){
             $users=User::all();
             $visitors=Visitor::all();
             $deletedUsers=User::onlyTrashed()->get();
            return view('all_users',compact('users','deletedUsers','visitors'));
        }
          
        return redirect('home');
    }

      public function settings()
    {
            
            return view('settings');
    }


    public function send(Request $request)
    {
        Validator::make($request->all(), [
            'mobile' => 'required|numeric',
            'message' => 'required',
        ])->validate();
        $msg=new Message();

        $msg->user_id= Auth::User()->id;
        $msg->mobile= $request->input('mobile');
        $msg->message= $request->input('message');
        $msg->save();

        session()->flash('feed_msg','The free trial of SMS messaging has expired.');

        return redirect('home');
    }

  


    public function deletemsg($id)
    {
        $msg=Message::find($id);
        $msg->delete();
        return redirect('msg');
    }

      public function deleteuser($id)
    {
        $msg=Message::where('user_id',$id)->delete();
        $num=Numberslist::where('user_id',$id)->delete();
        $user=User::find($id);
        $user->delete();
        return redirect('all_users');
    }
    public function ForceDeleteUser($id)
    {
        $msg=Message::where('user_id',$id)->delete();
        $num=Numberslist::where('user_id',$id)->delete();
        $user=User::onlyTrashed()->where('id', $id);
        $user->forceDelete();
        return redirect('all_users');
    }


    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|max:10',
            'mobile' => 'required|unique:numberslists|numeric',
            'email' => 'required|unique:numberslists|max:20|email',
        ])->validate();
         $list=new Numberslist();
         $list->name= $request->input('name');
         $list->user_id= Auth::User()->id;
         $list->email= $request->input('email');
         $list->mobile= $request->input('mobile');
         $list->save();
         return redirect('home');
    }


    public function update(Request $request, $id)
    {
        $list=Numberslist::find($id);
        $list->name= $request->input('name');
        $list->user_id= Auth::User()->id;
        $list->email= $request->input('email');
        $list->mobile= $request->input('mobile');
        $list->save();
        return redirect('home');
    }

    public function destroy($id)
    {
        $numbers=Numberslist::find($id);
        $numbers->delete();
        return redirect('home');
    }
// settings
public function updateAuth(Request $request)
{
    $sett=Auth::User();
    $sett->name= $request->input('name');
    $sett->email= $request->input('email');
    $sett->mobile= $request->input('mobile');
    $sett->country_code= $request->input('country_code');
    $sett->save();
    session()->flash('updateAuth','Your account has been successfully updated.');
    return redirect('settings');
}
public function deleteAuth()
{
    Auth::User()->delete();
    session()->flash('deleteAuth','Your account has been successfully deleted.');
    return redirect('login');
}

/*
      // Account details  (TextLocal api)
        $apiKey = urlencode('EwY6iQaTRjI-0GlcId4EY3FIMBNHBYl27taKyHKxEu');
        //$apiKey = urlencode('EwY6iQaTRjI-RcR00vFc64SvUVqnIfL2BqVMrVah0E');

        // Message details
        $numbers = array($request->input('mobile'));
        $sender = urlencode($request->input('name'));
        $message = rawurlencode($request->input('message'));

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

        // Send the POST request with cURL
        $ch = curl_init('https://api.txtlocal.com/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);


*/

// ----------------twilio api-----------------
    /*    
        $sid = "AC44d68019d05c869bd10a97cff915397b"; // Your Account SID from www.twilio.com/console
        $token = "93ec5fd91456da4d2ef34b2088b0589c"; // Your Auth Token from www.twilio.com/console

        //$client = new Twilio\Rest\Client($sid, $token);
        $client = new Client($sid,$token);
        $client->messages->create(
            $request->input('mobile'), // Text this number
            array(
                'from' => '+7313899017' , // From a valid Twilio number
                'body' => $request->input('message')
            )
        );
*/


/*   //--------------Nexmo API----
        Nexmo::message()->send([
            'to'   =>  $request->input('mobile'),
            'from' => '+212'.Auth::User()->mobile,
            'text' => $request->input('message')
        ]);
*/

}
